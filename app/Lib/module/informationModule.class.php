<?php
// +----------------------------------------------------------------------
// | 科创众达
// +----------------------------------------------------------------------
// | Copyright (c) 2011 http://ctrlcoo.com All rights reserved.
// +----------------------------------------------------------------------
// |  
// +----------------------------------------------------------------------

require APP_ROOT_PATH.'app/Lib/page.php';
class informationModule extends SiteBaseModule
{
	public function index()
	{
		$GLOBALS['tmpl']->caching = true;
		$cache_id  = md5(MODULE_NAME.ACTION_NAME.trim($_REQUEST['id']).intval($_REQUEST['p']));		
		if (!$GLOBALS['tmpl']->is_cached('page/acate_index.html', $cache_id))	
		{		
			$id = intval($_REQUEST['id']);
			$cate_item = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."information_cate where id = ".$id." and is_effect = 1 and is_delete = 0");
			
			if($id>0&&!$cate_item)
			{
				app_redirect(APP_ROOT."/");
			}
			// elseif($cate_item['type_id']!=0)
			// {
			// 	if($cate_item['type_id']==1)
			// 	app_redirect(url("index","help#index"));
			// 	if($cate_item['type_id']==2)
			// 	app_redirect(url("index","notice#list"));
			// 	if($cate_item['type_id']==3)
			// 	app_redirect(url("index","sys#alist"));
			// }
			
			$cate_id = intval($cate_item['id']);
			// print_r($cate_id);
			// exit();
			// $cate_tree = get_acate_tree();
			$cate_tree = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."information_cate where is_effect = 1 and is_delete = 0 order by `sort` desc, id desc");
			$GLOBALS['tmpl']->assign("acate_tree",$cate_tree);	
			$cate = null;
			foreach($cate_tree as $k=>$v){
				if($id == $v['id']){
					$cate = $v;
				}
			}		
			
			$GLOBALS['tmpl']->assign("cate",$cate);
	
			//分页
			$page = intval($_REQUEST['p']);
			if($page==0)
			$page = 1;
			$limit = (($page-1)*app_conf("PAGE_SIZE")).",".app_conf("PAGE_SIZE");		
			$result = $this->get_information_list($limit,$cate_id,'ac.type_id = 0','');
			// print_r($result['list']);
			// exit();
			$GLOBALS['tmpl']->assign("list",$result['list']);
			$page = new Page($result['count'],app_conf("PAGE_SIZE"));   //初始化分页对象 		
			$p  =  $page->show();
			$GLOBALS['tmpl']->assign('pages',$p);
			
			
			//开始输出当前的site_nav
			$cates = array();
			$cate = $cate_item;
			do
			{
				$cates[] = $cate;
				$pid = intval($cate['pid']);
				$cate = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."information_cate where is_effect =1 and is_delete =0 and id = ".$pid);			
				
			}while($pid!=0);
			
			foreach($cates as $cate_row)
			{
				$page_title .= $cate_row['title']." - "; 
				$page_kd .= $cate_row['title'].",";
			}
			$page_title = substr($page_title,0,-3);
			krsort($cates);
			
			$site_nav[] = array('name'=>$GLOBALS['lang']['HOME_PAGE'],'url'=>APP_ROOT."/");
			if($cate_item)
			{
				foreach($cates as $cate_row)
				{
					$site_nav[] = array('name'=>$cate_row['title'],'url'=>url("index","information#index",array("id"=>$cate_row['id'])));
				}
			}		
			else
			{
				$site_nav[] = array('name'=>$GLOBALS['lang']['ARTICLE_CATE'],'url'=>url("index","information#index"));
			}
			$GLOBALS['tmpl']->assign("site_nav",$site_nav);
			//输出当前的site_nav
			
			
			$GLOBALS['tmpl']->assign("page_title",$cate_item['title']);
			$GLOBALS['tmpl']->assign("page_keyword",$cate_item['title'].",");
			$GLOBALS['tmpl']->assign("page_description",$cate_item['title'].",");
		}
		$GLOBALS['tmpl']->display("page/information_index.html",$cache_id);
	}

	/**
	 * 获取信息列表
	 */
	function get_information_list($limit, $cate_id=0, $where='',$orderby = '',$cached = true)
	{		
			$key = md5("INFORMATION".$limit.$cate_id.$where.$orderby);
			if($cached)
			{				
				$res = $GLOBALS['cache']->get($key);
			}
			else
			{
				$res = false;
			}
			if($res===false)
			{
					
				$count_sql = "select count(*) from ".DB_PREFIX."information as a left join ".DB_PREFIX."information_cate as ac on a.cate_id = ac.id where a.is_effect = 1 and a.is_delete = 0 and ac.is_delete = 0 and ac.is_effect = 1 ";
				$sql = "select a.*,ac.type_id from ".DB_PREFIX."information as a left join ".DB_PREFIX."information_cate as ac on a.cate_id = ac.id where a.is_effect = 1 and a.is_delete = 0 and ac.is_delete = 0 and ac.is_effect = 1 ";
				
				if($cate_id>0)
				{
					// $ids = load_auto_cache("deal_shop_acate_belone_ids",array("cate_id"=>$cate_id));
					$ids = $cate_id;
					$sql .= " and a.cate_id in (".implode(",",$ids).")";
					$count_sql .= " and a.cate_id in (".implode(",",$ids).")";
				}
					
				
				if($where != '')
				{
					$sql.=" and ".$where;
					$count_sql.=" and ".$where;
				}
				
				if($orderby=='')
				$sql.=" order by a.sort desc limit ".$limit;
				else
				$sql.=" order by ".$orderby." limit ".$limit;
				
				$informations_count = $GLOBALS['db']->getOne($count_sql);
				$informations = array();
				if($informations_count > 0){
					$informations = $GLOBALS['db']->getAll($sql);	
					foreach($informations as $k=>$v)
					{
						// if($v['type_id']==1)
						// {
						// 	$module = "help";
						// }
						// elseif($v['type_id']==2)
						// {
						// 	$module = "notice";
						// }
						// elseif($v['type_id']==3)
						// {
						// 	$module = "sys";
						// }
						// else 
						// {
							$module = 'information_article';
						// }
						
						// if($v['uname']!='')
						// $aurl = url("index",$module,array("id"=>$v['uname']));
						// else
						$aurl = url("index",$module,array("id"=>$v['id']));
							
						$informations[$k]['url'] = $aurl;
					}
				}
					
				
		 		
				$res = array('list'=>$informations,'count'=>$informations_count);	
				$GLOBALS['cache']->set($key,$res);
			}			
			return $res;
	}

}
?>