<?php
// +----------------------------------------------------------------------
// | 科创众达
// +----------------------------------------------------------------------
// | Copyright (c) 2011 http://ctrlcoo.com All rights reserved.
// +----------------------------------------------------------------------
// |  
// +----------------------------------------------------------------------

require APP_ROOT_PATH.'app/Lib/page.php';
class information_articleModule extends SiteBaseModule
{
	public function index()
	{			
		$GLOBALS['tmpl']->caching = true;
		$cache_id  = md5(MODULE_NAME.ACTION_NAME.trim($_REQUEST['id']).$GLOBALS['deal_city']['id']);		
		if (!$GLOBALS['tmpl']->is_cached('page/article_index.html', $cache_id))	
		{
			// $cate_tree = get_acate_tree();
			$cate_tree = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."information_cate where is_effect = 1 and is_delete = 0 order by `sort` desc, id desc");		
			$GLOBALS['tmpl']->assign("acate_tree",$cate_tree);	
			
			$id = intval($_REQUEST['id']);
			$uname = addslashes(trim($_REQUEST['id']));
			
			if($id==0&&$uname=='')
			{
				app_redirect(APP_ROOT."/");
			}
			elseif($id==0&&$uname!='')
			{
				$id = $GLOBALS['db']->getOne("select id from ".DB_PREFIX."information where is_delete = 0 and is_effect = 1 and uname = '".$uname."'"); 
			}		
			// $information = get_information($id);
			$information = $GLOBALS['db']->getRow("select a.*,ac.type_id from ".DB_PREFIX."information as a left join ".DB_PREFIX."information_cate as ac on a.cate_id = ac.id where a.id = ".intval($id)." and a.is_effect = 1 and a.is_delete = 0");	
			$condition['click_count'] = $information['click_count'] + 1;
			$GLOBALS['db']->autoExecute(DB_PREFIX."information",$condition,"UPDATE","id=".$id);

			if(!$information||$information['type_id']!=0)
			{
				app_redirect(APP_ROOT."/");
			}	
			else
			{				
				if($information['rel_url']!='')
				{
					if(!preg_match ("/http:\/\//i", $information['rel_url']))
					{
						if(substr($information['rel_url'],0,2)=='u:')
						{					
									
							app_redirect(parse_url_tag($information['rel_url']));
						}
						else
						app_redirect(APP_ROOT."/".$information['rel_url']);
					}
					else
					app_redirect($information['rel_url']);
				}
			}		
			
			//开始输出当前的site_nav
			$cates = array();
			$cate = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."information_cate where id = ".$information['cate_id']);
			do
			{
				$cates[] = $cate;
				$pid = intval($cate['pid']);
				$cate = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."information_cate where is_effect =1 and is_delete =0 and id = ".$pid);			
				
			}while($pid!=0);
	
			$page_title = substr($page_title,0,-3);
			krsort($cates);
			
			$site_nav[] = array('name'=>$GLOBALS['lang']['HOME_PAGE'],'url'=>APP_ROOT."/");
			
			if($cates)
			{
				foreach($cates as $cate_row)
				{
					$site_nav[] = array('name'=>$cate_row['title'],'url'=>url("shop","acate#index",array("id"=>$cate_row['id'])));
					
				}
			}		
			if($information['uname']!='')
			{
				$aurl = url("shop","information#index",array("id"=>$information['uname']));
			}
			else
			{
				$aurl = url("shop","information#index",array("id"=>$information['id']));
			}
			$site_nav[] = array('name'=>$information['title'],'url'=>$aurl);
			$GLOBALS['tmpl']->assign("site_nav",$site_nav);
			//输出当前的site_nav
			
			// $information = get_information($id);
			$information = $GLOBALS['db']->getRow("select a.*,ac.type_id from ".DB_PREFIX."information as a left join ".DB_PREFIX."information_cate as ac on a.cate_id = ac.id where a.id = ".intval($id)." and a.is_effect = 1 and a.is_delete = 0");
			// print_r($information);
			// exit();
			$GLOBALS['tmpl']->assign("article",$information);
			$seo_title = $information['seo_title']!=''?$information['seo_title']:$information['title'];
			$GLOBALS['tmpl']->assign("page_title",$seo_title);
			$seo_keyword = $information['seo_keyword']!=''?$information['seo_keyword']:$information['title'];
			$GLOBALS['tmpl']->assign("page_keyword",$seo_keyword.",");
			$seo_description = $information['seo_description']!=''?$information['seo_description']:$information['title'];
			$GLOBALS['tmpl']->assign("page_description",$seo_description.",");

			//留言			
			$rel_table = 'information';
			$condition = "rel_table = '".$rel_table."' and rel_id = ".$id;
			//message_form 变量输出
			$GLOBALS['tmpl']->assign('rel_id',$id);
			$GLOBALS['tmpl']->assign('rel_table',$rel_table);
			
			//分页
			$page = intval($_REQUEST['p']);
			if($page==0)
			$page = 1;
			$limit = (($page-1)*app_conf("PAGE_SIZE")).",".app_conf("PAGE_SIZE");
			$msg_condition = $condition." AND is_effect = 1 ";
			$message = $this->get_message_list($limit,$msg_condition);
			
			$page = new Page($message['count'],app_conf("PAGE_SIZE"));   //初始化分页对象 		
			$p  =  $page->show();
			$GLOBALS['tmpl']->assign('pages',$p);
			foreach($message['list'] as $k=>$v){
				$msg_sub = $this->get_message_list("","pid=".$v['id'],false);
				$message['list'][$k]["sub"] = $msg_sub["list"];
			}
			
			$GLOBALS['tmpl']->assign("message_list",$message['list']);
			if(!$GLOBALS['user_info'])
			{
				$GLOBALS['tmpl']->assign("message_login_tip",sprintf($GLOBALS['lang']['MESSAGE_LOGIN_TIP'],url("shop","user#login"),url("shop","user#register")));
			}
		}
		$GLOBALS['tmpl']->display("page/information_article_index.html",$cache_id);
	}
	
	//评论
	public function add()
	{		

		$user_info = $GLOBALS['user_info'];
		$ajax = intval($_REQUEST['ajax']);
		if(!$user_info)
		{
			showErr($GLOBALS['lang']['PLEASE_LOGIN_FIRST'],$ajax);
		}
		if($_REQUEST['content']=='')
		{
			showErr($GLOBALS['lang']['MESSAGE_CONTENT_EMPTY'],$ajax);
		}
		
		//验证码
		if(app_conf("VERIFY_IMAGE")==1)
		{	
			require APP_ROOT_PATH."system/utils/Verify.class.php";

			// $verify = md5(trim($_REQUEST['verify']));
			// $session_verify = es_session::get('verify');
			// if($verify!=$session_verify)
			$verify = new Verify(array());
			$res=$verify->check ($_REQUEST['verify'] , 'verify');
			if (!$res)
			{				
				showErr($GLOBALS['lang']['VERIFY_CODE_ERROR'],$ajax);
			}
		}
		
		if(!check_ipop_limit(CLIENT_IP,"message",intval(app_conf("SUBMIT_DELAY")),0))
		{
			showErr($GLOBALS['lang']['MESSAGE_SUBMIT_FAST'],$ajax);
		}
		
		$rel_id = strim($_REQUEST['rel_id']);
		$information = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."information where id='".$rel_id."'");
		if(!$information)
		{
			showErr($GLOBALS['lang']['INVALID_MESSAGE_TYPE'],$ajax);
		}			
		//添加留言
		$message['title'] = $_REQUEST['title']?strim($_REQUEST['title']):strim($_REQUEST['content']);
		
		$message['content'] = strim($_REQUEST['content']);
		$message['title'] = valid_str($message['title']);
		$message['content'] = valid_str($message['content']);
			
		$message['create_time'] = TIME_UTC;
		$message['rel_table'] = $rel_table;
		$message['rel_id'] = intval($_REQUEST['rel_id']);
		$message['rel_table'] = 'information';
		$message['user_id'] = intval($GLOBALS['user_info']['id']);
		
		if(app_conf("USER_MESSAGE_AUTO_EFFECT")==0)
		{
			$message_effect = 0;
		}
		else
		{
			$message_effect = $information['is_effect'];
		}
		$message['is_effect'] = $message_effect;		
		// print_r($message);
		// exit();
		$GLOBALS['db']->autoExecute(DB_PREFIX."information_message",$message);
		// $l_user_id =  $GLOBALS['db']->getOne("SELECT user_id FROM ".DB_PREFIX."deal WHERE id=".$message['rel_id']);
		
		// //添加到动态
		// insert_topic($rel_table."_message",$message['rel_id'],$message['user_id'],$GLOBALS['user_info']['user_name'],$l_user_id);
		
		// if($rel_table == "deal"){
		// 	require_once APP_ROOT_PATH.'app/Lib/deal.php';
		// 	$deal = get_deal($message['rel_id']);
		// 	//自己给自己留言不执行操作
		// 	if($deal['user_id']!=$message['user_id']){
		// 		$msg_conf = get_user_msg_conf($deal['user_id']);
		// 		//站内信
		// 		if($msg_conf['sms_asked']==1){
		// 			$notices['user_name'] = get_user_name($message['user_id']);
		// 			$notices['url'] = " “<a href=\"".$deal['url']."\">".$deal['name']."</a>”";
		// 			$notices['msg'] = $message['content'];
						
		// 			$tmpl_content = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."msg_template where name = 'TPL_WORDS_MSG'",false);
		// 			$GLOBALS['tmpl']->assign("notice",$notices);
		// 			$contents = $GLOBALS['tmpl']->fetch("str:".$tmpl_content['content']);
						
		// 			send_user_msg("",$contents,0,$deal['user_id'],TIME_UTC,0,true,13,$message['rel_id']);
		// 		}
		// 		//邮件
		// 		if($msg_conf['mail_asked']==1 && app_conf('MAIL_ON')==1){
		// 			$user_info = get_user_info("*","id = ".$deal['user_id']);
		// 			$tmpl = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."msg_template where name = 'TPL_MAIL_DEAL_MSG'",false);
		// 			$tmpl_content = $tmpl['content'];
					
		// 			$notice['user_name'] = $user_info['user_name'];
		// 			$notice['msg_user_name'] = get_user_name($message['user_id'],false);
		// 			$notice['deal_name'] = $deal['name'];
		// 			$notice['deal_url'] = SITE_DOMAIN.url("index","deal",array("id"=>$deal['id']));
		// 			$notice['message'] = $message['content'];
		// 			$notice['site_name'] = app_conf("SHOP_TITLE");
		// 			$notice['site_url'] = SITE_DOMAIN.APP_ROOT;
		// 			$notice['help_url'] = SITE_DOMAIN.url("index","helpcenter");
					
					
		// 			$GLOBALS['tmpl']->assign("notice",$notice);
					
		// 			$msg = $GLOBALS['tmpl']->fetch("str:".$tmpl_content);
		// 			$msg_data['dest'] = $user_info['email'];
		// 			$msg_data['send_type'] = 1;
		// 			$msg_data['title'] = get_user_name($message['user_id'],false)."给您的标留言！";
		// 			$msg_data['content'] = addslashes($msg);
		// 			$msg_data['send_time'] = 0;
		// 			$msg_data['is_send'] = 0;
		// 			$msg_data['create_time'] = TIME_UTC;
		// 			$msg_data['user_id'] = $user_info['id'];
		// 			$msg_data['is_html'] = $tmpl['is_html'];
		// 			$GLOBALS['db']->autoExecute(DB_PREFIX."deal_msg_list",$msg_data); //插入
		// 		}
		// 	}
		// }
		
		showSuccess($GLOBALS['lang']['MESSAGE_POST_SUCCESS'],$ajax);
	}

	function get_message_list($limit,$where='',$is_top = true)
	{
		$sql = "select * from ".DB_PREFIX."information_message where 1=1 ";
		
		if($is_top){
			$sql_count = "select count(*) from ".DB_PREFIX."information_message where 1=1";
			$sql .= " and pid=0 ";
			$sql_count .=  " and pid=0 ";
		}
		
		if($where!='')
		{
			$sql .= " and ".$where;
			$sql_count .=  " and ".$where;
		}
		
		$sql.=" order by create_time desc ";
		
		if($is_top){
			$sql.=" limit ".$limit;
			$count = $GLOBALS['db']->getOne($sql_count);
		}

		$list = $GLOBALS['db']->getAll($sql);
		
		return array('list'=>$list,'count'=>$count);
	}


	// public function alist()
	// {
	// 	$GLOBALS['tmpl']->caching = true;
	// 	$cache_id  = md5(MODULE_NAME."list".strim($_REQUEST['id']).intval($_REQUEST['p']).$GLOBALS['deal_city']['id']);		
	// 	if (!$GLOBALS['tmpl']->is_cached('page/article_list.html', $cache_id))	
	// 	{
	// 		// $cate_list = get_acate_tree();
	// 		$cate_tree = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."information_cate where is_effect = 1 and is_delete = 0 order by `sort` desc, id desc");
	// 		$GLOBALS['tmpl']->assign('cate_list',$cate_list);
	// 		//分页
	// 		$page = intval($_REQUEST['p']);
	// 		if($page==0)
	// 		$page = 1;
	// 		$limit = (($page-1)*app_conf("PAGE_SIZE")).",".app_conf("PAGE_SIZE");
			
	// 		$cate = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."article_cate where id = ".intval($_REQUEST['id']));
	// 		$GLOBALS['tmpl']->assign('cate',$cate);
			
	// 		$result = get_article_list($limit,intval($_REQUEST['id']),'','',true);
			
	// 		$GLOBALS['tmpl']->assign("list",$result['list']);
	// 		$page = new Page($result['count'],app_conf("PAGE_SIZE"));   //初始化分页对象 		
	// 		$p  =  $page->show();
	// 		$GLOBALS['tmpl']->assign('pages',$p);
			
	// 		//开始输出当前的site_nav			
	// 		$site_nav[] = array('name'=>$GLOBALS['lang']['HOME_PAGE'],'url'=>APP_ROOT."/");
	// 		$site_nav[] = array('name'=>$GLOBALS['lang']['SHOP_SYSTEM'],'url'=>url("shop","sys#list"));
	// 		$GLOBALS['tmpl']->assign("site_nav",$site_nav);
	// 		//输出当前的site_nav
			
	// 		$GLOBALS['tmpl']->assign('page_title',$cate['title']);
	// 		$GLOBALS['tmpl']->assign('page_keyword',$GLOBALS['lang']['SHOP_SYSTEM']);
	// 		$GLOBALS['tmpl']->assign('page_description',$GLOBALS['lang']['SHOP_SYSTEM']);
	// 	}
	// 	$GLOBALS['tmpl']->display("page/article_list.html",$cache_id);
	// }
}
?>